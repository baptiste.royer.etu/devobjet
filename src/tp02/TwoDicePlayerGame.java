class TwoDicePlayerGame {
    public static final Read read = new Read();

    public static DicePlayer player1;
    public static Dice dice1;
    public static DicePlayer player2;
    public static Dice dice2;



    public static void main(String[] args) {
        System.out.print("Entrez votre nom : ");
        player1 = new DicePlayer(read.readString());
        System.out.print("Le nombre de faces : ");
        dice1 = new Dice(read.readInt());

        System.out.print("Entrez votre nom : ");
        player2 = new DicePlayer(read.readString());
        System.out.print("Le nombre de faces : ");
        dice2 = new Dice(read.readInt());
        while (player1.totalValue < 20 || player2.totalValue < 20) {
            if (player1.totalValue < 20) {
                player1.play(dice1);
            }
            if (player2.totalValue < 20) {
                player2.play(dice2);
            }
        }

        System.out.println("Le gagnant est " + winner().toString());
    }

    public static DicePlayer winner() {
        if (player1.nbDiceRolls < player2.nbDiceRolls) {
            return player1;
        } else {
            return player2;
        }
    }
}