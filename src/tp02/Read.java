import java.util.Scanner;
class Read {
    public static Scanner scanner = new Scanner(System.in);
    
    public static int readInt() {
        int scan = scanner.nextInt();
        return scan;
    }

    public static String readString() {
        String scan = scanner.nextLine();
        return scan;
    }
}