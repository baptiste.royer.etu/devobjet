import java.util.Random;
class Dice {
    int numberSides;
    int value;
    Random rand;

    Dice(int faces) {
        if (faces < 1) {
            this.numberSides = 1;
        } else {this.numberSides = faces;}
        this.rand = new Random();
        roll();
    }

    public void roll() {
        this.value = rand.nextInt(1, this.numberSides);
    }

    public String toString() {
        return "" + value;
    }
}