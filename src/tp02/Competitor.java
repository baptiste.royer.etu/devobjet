class Competitor {
    public String numberSign;
    int time;
    int score;

    Competitor(int numberSign, int score, int min, int sec) {
        if (numberSign>100||numberSign<1||min<0||min>59||sec<0||sec>60||score<0||score>50) {this.numberSign=null;}
        else {
            this.numberSign = "No" + numberSign;
            this.score = score;
            this.time = sec + (min*60);
        }
    }

    public String display() {
        String print = "[";
        if (this.numberSign == null) {
            print = print + "<invalide>";
        } else {
            print = print + this.numberSign;
        }
        print = print + ", " + this.score + " points, " + this.time + " s]";
        return print;
    }

    public static void main(String[] args) {
        Competitor[] table = new Competitor[100];
        table[0] = new Competitor(1, 45, 15, 20);
        table[1] = new Competitor(2, 32, 12, 45);
        table[2] = new Competitor(5, 12, 13, 59);
        table[3] = new Competitor(12, 12, 15, 70);
        table[4] = new Competitor(32, 75, 15, 20);
        int i = 0;
        while (table[i] != null) {
            if (table[i].numberSign == null) {
                i++;
            } else {
                System.out.println(table[i].display());
                i++;
            }
        }
    }
}