class NbDicePlayerGame {
    public static final Read read = new Read();

    public static DicePlayer[] players;
    public static Dice[] dices;



    public static void main(String[] args) {
        System.out.print("Combien de joueurs : ");
        int nb = read.readInt();
        players = new DicePlayer[nb];
        dices = new Dice[nb];

        for (int i = 0; i < players.length; i++) {
            DicePlayer player = new DicePlayer("Michelle" + i);
            Dice dice = new Dice(6);
            players[i] = player;
            dices[i] = dice;
        }
        
        while (!finished()) {
            for (int i = 0; i < players.length; i++) {
                if (players[i].totalValue < 20) {
                    players[i].play(dices[i]);
                }
            }
        }

        System.out.println("Le(s) gagnant(s) est/sont : ");
        DicePlayer[] winners = winner();
        int i = 0;
        while (winners[i] != null && i<winners.length) {
            System.out.println("- " + winners[i].toString());
            i++;
        }
    }

    public static DicePlayer[] winner() {
        int[] scores = new int[players.length];
        DicePlayer[] winners = new DicePlayer[players.length];
        int min = players[0].nbDiceRolls;
        for (int i = 1; i<players.length;i++) {
            if (players[i].nbDiceRolls < min) {
                min = players[i].nbDiceRolls;
            }
        }
        int w = 0;
        for (int i = 0; i<players.length;i++) {
            if (players[i].nbDiceRolls == min) {
                winners[w] = players[i];
                w++;
            }
        }
        return winners;
    }

    public static boolean finished() {
        boolean retour = true;
        int i = 0;
        while (retour && i<players.length) {
            if (players[i].totalValue<19) {
                retour = false;
            }
            i++;
        }
        return retour;
    }
}