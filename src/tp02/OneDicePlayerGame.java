class OneDicePlayerGame {
    public static final Read read = new Read();

    public static void main(String[] args) {
        System.out.print("Entrez votre nom : ");
        DicePlayer player = new DicePlayer(read.readString());
        System.out.print("Le nombre de faces : ");
        Dice dice = new Dice(read.readInt());
        while (player.totalValue < 20) {
            player.play(dice);
        }
        System.out.println(player.toString());
    }
}