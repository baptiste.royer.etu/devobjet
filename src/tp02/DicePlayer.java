class DicePlayer {
    String name;
    int totalValue;
    int nbDiceRolls;

    DicePlayer(String name) {
        this.name = name;
        this.totalValue = 0;
        this.nbDiceRolls = 0;
    }

    public void play(Dice de) {
        de.roll();
        this.totalValue += Integer.parseInt(de.toString());
        this.nbDiceRolls++;
    }

    public String toString() {
        return this.name + ": " + this.totalValue + " en " + this.nbDiceRolls + " coups.";
    }
}