package tp09;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.File;  // Import the File class
import java.io.FileWriter;   // Import the FileWriter class

public class DicoJava {

    public static int getIndiceTabulation(String line) {
        int i = 0;
        while (i < line.length() && line.charAt(i) != '\t') {
            i++;
        }
        return i;
    }

	public static void main(String[] args) {
		
		try {
			// Création d'un fileReader pour lire le fichier
			FileReader fileReader = new FileReader("res/tp09/DicoJava.txt");

      // Création d'un fichier
      FileWriter myWriter = new FileWriter("MotsJava.txt");

			// Création d'un bufferedReader qui utilise le fileReader
			BufferedReader reader = new BufferedReader(fileReader);
			
			// une fonction à essayer pouvant générer une erreur
			String line = reader.readLine();
			
			while (line != null) {
				// affichage de la ligne
        line = line.substring(0, getIndiceTabulation(line));
        myWriter.write(line + "\n");
				// lecture de la prochaine ligne
				line = reader.readLine();
			}
			reader.close();
      myWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
/*
import java.io.File;  // Import the File class
import java.io.IOException;  // Import the IOException class to handle errors

public class CreateFile {
  public static void main(String[] args) {
    try {
      File myObj = new File("filename.txt");
      if (myObj.createNewFile()) {
        System.out.println("File created: " + myObj.getName());
      } else {
        System.out.println("File already exists.");
      }
    } catch (IOException e) {
      System.out.println("An error occurred.");
      e.printStackTrace();
    }
  }
}
*/
