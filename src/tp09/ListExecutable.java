package tp09;

import java.io.File;
import java.util.Scanner;

public class ListExecutable {
    public static void printExecutable(File file) {
        if (file.exists()) {
            if(file.isFile() && !file.isHidden()) {
                if(file.canExecute()) {
                    System.out.println(file.getName());
                } else {
                    System.out.println("Nothing is executable.");
                }
            } else {
                File[] li = file.listFiles();
                for (File fichier : li) {
                    if(fichier.isFile() && !file.isHidden()) {
                        if(fichier.canExecute()) {
                            System.out.println(fichier.getName());
                        }
                    }
                }
            }
        } else {
            System.out.println("File does not exist.");
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Veuillez entrer le nom du fichier ou du dossier que vous souhaitez vérifier : ");
        File file = new File(scanner.nextLine());
        printExecutable(file);
        scanner.close();
    }
}