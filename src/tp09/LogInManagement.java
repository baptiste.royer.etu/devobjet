package tp09;
import java.util.Scanner;

public class LogInManagement {
    public static final String username = "admin";
    public static final String password = "admin";

    public static void getUserPWD() throws WrongPWDException, WrongUserException, WrongLengthException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your username: ");
        String enteredUsername = scanner.nextLine();
        System.out.println("Enter your password: ");
        String enteredPassword = scanner.nextLine();
        if (enteredUsername.equals(username) && enteredPassword.equals(password)) {
            System.out.println("Welcome " + username + "!");
        } else {
            if(enteredPassword.length() > 10 || enteredUsername.length() > 10) {
                throw new WrongLengthException("Username and password must be less than 10 characters!");
            } else {
                if (!enteredUsername.equals(username) && !enteredPassword.equals(password)) {
                    throw new WrongUserException("Wrong username and password!");
                } else if (!enteredUsername.equals(username)) {
                    throw new WrongUserException("Wrong username!");
                } else {
                    throw new WrongPWDException("Wrong password!");
                }
            }
        }
    }

    public static void main(String[] args) throws WrongPWDException, WrongUserException, WrongLengthException {
        getUserPWD();
    }

    
}