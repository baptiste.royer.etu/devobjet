package tp09;

public class WrongLengthException extends Exception {
    public WrongLengthException(String message) {
        super(message);
    }
}