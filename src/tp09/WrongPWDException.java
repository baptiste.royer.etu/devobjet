package tp09;

public class WrongPWDException extends Exception {
    public WrongPWDException(String message) {
        super(message);
    }
}