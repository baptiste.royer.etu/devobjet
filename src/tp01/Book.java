
class Book {
    // class attributes
    String author;
    String title;
    int year;
    // constructor
    Book(String author, String title, int year) {
        this.author = author;
        this.title = title;
        this.year = year;
    }
    // methods
    String getAuthor() {
        return this.author;
    }
    String getTitle() {
        return this.title;
    }
    String print() {
        return author + "\t" + title + "\t" + year;
    }

    public static void main(String[] args) {
        Book buch = new Book("Edwin A. Abbott", "Flatland", 1884);
        System.out.println(buch.author + " a écrit " + buch.title + " en " + buch.year + ".");
    }
}