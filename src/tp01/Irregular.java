class Irregular {
    int[][] table;

    Irregular(int[] lineSize) {
        this.table = new int[lineSize.length][];
        for (int i = 0; i < lineSize.length; i++) {
            this.table[i] = new int[lineSize[i]];
        }
    }

    void randomFilling() {
        for(int i = 0; i < this.table.length; i++) {
            for (int j = 0; j < this.table[i].length; j++) {
                this.table[i][j] = (int)(Math.random()*10);
            }
        }
    }

    String display() {
        String retour = "";
        for(int i = 0; i < this.table.length; i++) {
            for (int j = 0; j < this.table[i].length; j++) {
                retour = retour + this.table[i][j] + " ";
            }
            retour = retour + "\n";
        }
        return retour;
    }

    boolean isCommon(int integer) {
        int verif = 0;
        for (int i = 0; i < this.table.length; i++) {
            int j = 0;
            boolean found = false;
            while(j < this.table[i].length && found == false) {
                if (this.table[i][j] == integer) {
                    verif++;
                    found = true;
                }
                j++;
            }
        }
        return verif == this.table.length;
    }

    boolean existCommon() {
        int[] possibleEntrys = this.table[0];
        int[] oui = new int[possibleEntrys.length];
        for (int w = 0; w<oui.length; w++) {
            oui[w] = 0;
        }
        for (int i = 0; i<possibleEntrys.length; i++) {
            for (int j = 0; j<this.table.length; j++) {
                int k = 0;
                boolean found = false;
                while (k < this.table[j].length && found == false) {
                    if (this.table[j][k] == possibleEntrys[i]) {
                        oui[i]++;
                        found = true;
                    }
                    k++;
                }
            }
        }
        for (int l = 0; l<oui.length;l++) {
            if (oui[l] == this.table.length) {
                return true;
            }
        }
        return false;
    }
}