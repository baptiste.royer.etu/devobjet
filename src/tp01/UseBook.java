class UseBook {
    public static void main(String[] args) {
        Book[] biblio = new Book[]{new Book("Ma in", "Kampf", 1934), new Book("Hoa", "Lache", 1820), new Book("Cond", "Amener", 1346)};
        printBooks(biblio);
    }

    public static void printBooks(Book[] biblio) {
        for (Book book : biblio) {
            System.out.println(book.print());
        }
    }
}