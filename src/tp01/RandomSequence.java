class RandomSequence {
    public static void main(String[] args) {
        if (args.length==0) {
            System.out.println("No parameter");
        } else if (args.length<2||args.length>3||(args.length==3&&!args[2].equals("REAL")&&!args[2].equals("INTEGER"))) {
            System.out.println("Correct usage : <nbElt> <maxVal> [INTEGER|REAL]");
        }
        for (int i = 0; i<Integer.parseInt(args[0]);i++) {
            if (args.length==3 && args[2].equals("REAL")) {
                System.out.println(Math.random()*Integer.parseInt(args[1]));
            } else {
                System.out.println((int)(Math.random()*Integer.parseInt(args[1])));
            }
        }
    }
}