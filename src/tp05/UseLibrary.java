package tp05;

public class UseLibrary {
    public static void main(String[] args) {
        Library bookstore = new Library();
        Book book1 = new Book("LGDC", "La Guerre Des Clans", "Erin Hunter", 2003);
        Book book2 = new Book("ERGN", "Eragon", "Christopher Paolini", 2002);
        Book book3 = new Book("LMDO", "Les Métamprphoses D'Ovide", "Ovide", 100);
        ComicBook book4 = new ComicBook("TAT", "Tintin Au Tibet", "Hergé", "Hergé", 1960);
        int borrower1 = 42;
        int borrower2 = 404;

        boolean add1 = bookstore.addBook(book1);
        boolean add2 = bookstore.addBook(book2);
        boolean add3 = bookstore.addBook(book3);
        boolean add4 = bookstore.addBook(book4);

        Book search = bookstore.getBook("ERGN");

        String script = bookstore.toString();

        System.out.println(add1);
        System.out.println(add2);
        System.out.println(add3);
        System.out.println(add4);

        newLine();

        System.out.println(search.toString());

        newLine();

        System.out.println(script);

        newLine();

        System.out.println(bookstore.borrow("ERGN", borrower1) + " --> " + bookstore.borrowings());
        System.out.println(bookstore.borrow("ERGN", borrower2) + " --> " + bookstore.borrowings());
        System.out.println(bookstore.borrow("LGDC", borrower2) + " --> " + bookstore.borrowings());
        System.out.println(bookstore.borrow("LMDO", borrower1) + " --> " + bookstore.borrowings());

        newLine();

        System.out.println(bookstore.toString());
    }

    public static void newLine() {
        System.out.println();
    }
}