package tp05;

public class ComicBook extends Book {
    private String illustrator;

    public ComicBook(String code, String title, String author, String illustrator, int publicationYear) {
        super(code, title, author, publicationYear);
        this.illustrator = illustrator;
    }

    public String toString() {
        String borrowed = "";
        if (super.getBorrowingDate() != null) {
            borrowed = " borrowed the : " + super.getBorrowingDate().toString();
        }
        return "ComicBook [" + super.getCode() + ":" + super.getTitle() + " -> " + super.getAuthor() + ", " + super.getPublicationYear() + ", " + this.illustrator + "]" + borrowed;
    }
}
