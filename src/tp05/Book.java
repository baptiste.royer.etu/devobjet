package tp05;

import java.time.LocalDateTime;

public class Book {
    private String code;
    private String title;
    private String author;
    private int publicationYear;
    private int borrower = 0;
    private LocalDateTime borrowingDate = null;

    public Book(String code, String title, String author, int publicationYear) {
        this.code = code;
        this.title = title;
        this.author = author;
        this.publicationYear = publicationYear;
    }

    public String getCode() {
        return this.code;
    }

    public String getTitle() {
        return this.title;
    }

    public String getAuthor() {
        return this.author;
    }

    public int getPublicationYear() {
        return this.publicationYear;
    }

    public int getBorrower() {
        return this.borrower;
    }

    public LocalDateTime getBorrowingDate() {
        return this.borrowingDate;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setPublicationYear(int publicationYear) {
        this.publicationYear = publicationYear;
    }

    public boolean borrow(int borrower) {
        if (this.borrower == 0) {
            this.borrower = borrower;
            this.borrowingDate = LocalDateTime.now();
            return true;
        } else {
            return false;
        }
    }

    public boolean giveBack() {
        if (this.borrower != 0) {
            this.borrower = 0;
            this.borrowingDate = null;
            return true;
        } else {
            return false;
        }
    }

    public LocalDateTime getGiveBackDate() {
        return this.borrowingDate.plusDays(10);
    }

    public boolean isAvailable() {
        return this.borrower == 0;
    }

    public String toString() {
        String borrowed = "";
        if (this.borrowingDate != null) {
            borrowed = " borrowed the : " + this.borrowingDate.toString();
        }
        return "Book [" + this.getCode() + ":" + this.title + " -> " + this.author + ", " + this.publicationYear + "]" + borrowed;
    }
}