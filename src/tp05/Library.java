package tp05;

public class Library {
    private Book[] catalog = new Book[0];

    public Book getBook(String code) {
        int i = 0;
        while (i<this.catalog.length && this.catalog[i].getCode() != code) {
            i++;
        }
        return catalog[i];
    }

    public boolean addBook(Book book) {
        Book[] temp = catalog;
        this.catalog = new Book[temp.length+1];
        for (int i = 0; i < temp.length; i++) {
            this.catalog[i] = temp[i];
        }
        this.catalog[temp.length] = book;
        return true;
    }

    public boolean removeBook(String aCode) {
        Book remove = getBook(aCode);
        int indice = getIndice(remove);
        Book[] temp = this.catalog;
        this.catalog = new Book[temp.length-1];
        int index = 0;
        for (int i = 0; i<this.catalog.length; i++) {
            if(i != indice) {
                this.catalog[index] = temp[i];
                index++;
            }
        }
        return true;
    }

    public boolean removeBook(Book b) {
        int indice = getIndice(b);
        Book[] temp = this.catalog;
        this.catalog = new Book[temp.length-1];
        int index = 0;
        for (int i = 0; i<this.catalog.length; i++) {
            if(i != indice) {
                this.catalog[index] = temp[i];
                index++;
            }
        }
        return true;
    }

    public int getIndice(Book book) {
        int i = 0;
        while (i<this.catalog.length && this.catalog[i] != book) {
            i++;
        }
        return i;
    }

    public String borrowings() {
        String result = "";
        for (int i = 0; i<this.catalog.length;i++) {
            if (this.catalog[i].getBorrower() != 0) {
                result += "(" + this.catalog[i].getCode() + ")--" + this.catalog[i].getBorrower() + " ";
            }
        }
        return result;
    }

    public boolean borrow(String code, int borrower) {
        Book book = getBook(code);
        return (book.borrow(borrower));
    }

    public boolean giveBack(String code) {
        Book book = getBook(code);
        return book.giveBack();
    }

    public String toString() {
        String result = "[";
        for (int i = 0; i<this.catalog.length; i++) {
            result += this.catalog[i].toString() + "\n";
        }
        return result.substring(0, result.length()-1) + "]";
    }
    
}