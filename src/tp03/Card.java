package tp03;

class Card {
    private Color color;
    private Rank rank;

    public Card(Color color, Rank rank) {
        this.color = color;
        this.rank = rank;
    }

    public Card(String color, String rank) {
        this.color = Color.valueOf(color);
        this.rank = Rank.valueOf(rank);
    }

    public Color getColor() {
        return this.color;
    }

    public Rank getRank() {
        return this.rank;
    }

    public int compareRank(Card card) {
        return this.rank.getValeur() - card.rank.getValeur();
    }

    public int compareColor(Card card) {
        return this.color.compareTo(card.getColor());
    }

    public boolean isBefore(Card card) {
        return this.rank.getValeur() - card.rank.getValeur() < 0;
    }

    public boolean equals(Card card) {
        return this.compareColor(card) == 0 && this.compareRank(card) == 0;
    }

    public String toString() {
        return this.rank.toString() + " of " + this.color.toString();
    }
}