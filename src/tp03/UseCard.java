package tp03;

class UseCard {
    public static void main(String[] args) {
        Card carte1 = new Card(Color.SPADE, Rank.SEVEN);
        Card carte2 = new Card("HEART", "KING");
        System.out.println(carte1.getColor());
        System.out.println(carte2.getRank());
        System.out.println(carte1.isBefore(carte2));
        System.out.println(carte1.equals(carte2));
        System.out.println(carte1.toString() + "\n" + carte2.toString());
    }
}