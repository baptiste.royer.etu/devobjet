package tp03;
import java.time.LocalDate;
import java.util.Scanner;

class UseLocalDate {
    private Scanner scan = new Scanner(System.in);

    public static String dateOfTheDay() {
        return "Today's date is " + LocalDate.now() + ".";
    }

    public static void main(String[] args) {
        System.out.println(dateOfTheDay());
    }

    public static LocalDate inputDate() {
        return null;
    }
}