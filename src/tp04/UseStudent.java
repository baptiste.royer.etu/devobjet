package tp04;

public class UseStudent {
    public static void main(String[] args) {
        double[] grades = new double[] {19.25, 15.2, 0.01};
        double[] grades2 = new double[] {5, 2849, 9};
        Person gens1 = new Person("AAAAAAAAAAAAAAAAAAA", "BBBBB");
        Student gars1 = new Student(gens1, grades);
        Student gars2 = new Student("Jean-Marie", "Lepen", grades2);
        Student gars3 = new Student("Gicé", "Védé");
        gars3.addGrade(5.49);
        gars3.addGrade(5.49);
        gars3.addGrade(5.49);
        System.out.println(gars1.equals(gars2));
        System.out.println(gars2.equals(gars3));
        System.out.println(gars1.toString());
        System.out.println(gars2.toString());
        System.out.println(gars3.toString());
    }
}