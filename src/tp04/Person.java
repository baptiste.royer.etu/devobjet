package tp04;


public class Person {
    private int ID;
    private String forename;
    private String name;

    public Person(String forename, String name) {
        this.forename = forename;
        this.name = name;
        NumeroAutomatique temp = new NumeroAutomatique();
        this.ID = temp.getNumber();
    }

    public int getID() {
        return this.ID;
    }

    public String getForename() {
        return this.forename;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setForename(String forename) {
        this.forename = forename;
    }

    public String toString() {
        return ID + " : " + forename + " " + name;
    }

    public boolean equals(Person other) {
        return this.ID == other.ID;
    }
}