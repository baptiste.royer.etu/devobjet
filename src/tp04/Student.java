package tp04;

public class Student{
    private Person person;
    private double[] grades;

    public Student(Person pers, double[] grades) {
        this.person = pers;
        this.grades = grades;
    }

    public Student(String forename, String name, double[] grades) {
        this.person = new Person(forename, name);
        this.grades = grades;
    }

    public Student(String forename, String name) {
        this.person = new Person(forename, name);
        this.grades = new double[0];
    }

    public String getForename() {
        return this.person.getForename();
    }

    public String getName() {
        return this.person.getName();
    }

    public int getID() {
        return this.person.getID();
    }

    public double[] getGrades() {
        return this.grades;
    }

    public String getStringGrades() {
        String result = "[";
        for (int i = 0; i<this.grades.length; i++) {
            result += this.grades[i] + ";";
        }
        result = result.substring(0, result.length() - 1);
        return result + "]";
    }

    public void setForename(String forename) {
        this.person.setForename(forename);
    }

    public void setName(String name) {
        this.person.setName(name);
    }

    public void setGrades(double[] grades) {
        this.grades = grades;
    }

    public String toString() {
        return "Student [" + this.person.getID() + " : " + this.person.getForename() + " " + this.person.getName() + " = " + getStringGrades() + "; avg = " + getAverage() + "]";
    }

    public boolean equals(Student other) {
        return (this.person == other.person && this.grades == other.grades);
    }

    public double getAverage() {
        if (grades.length == 0) {
            return 0;
        } else {
            int somme = 0;
            for (int i = 0; i<grades.length; i++) {
                somme += grades[i];
            }
            return somme/grades.length;
        }
    }

    public void addGrade(double grade) {
        double[] temp = this.grades;
        this.grades = new double[temp.length + 1];
        for (int i = 0; i<grades.length; i++) {
            if (i<temp.length) {
                this.grades[i] = temp[i];
            } else {
                this.grades[i] = grade;
            }
        }
    }
}