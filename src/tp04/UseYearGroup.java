package tp04;

public class UseYearGroup {
    public static void main(String[] args) {
        Student gars1 = new Student("Clara", "Oké", new double[] {19.25, 15.2, 17.3});
        Student gars2 = new Student("Will", "Kinson", new double[] {5, 13, 2});
        Student gars3 = new Student("Titouan", "Ladopté", new double[] {17.6, 14.3, 15.9});
        Student gars4 = new Student("Gicé", "Védé", new double[] {0, 0.1, 0.2});
        StudentAbs abs1 = new StudentAbs(gars1);
        StudentAbs abs2 = new StudentAbs(gars2);
        StudentAbs abs3 = new StudentAbs(gars3);
        StudentAbs abs4 = new StudentAbs(gars4);
        for (int i = 0; i < 10; i++) {
            abs3.increaseAbs();
            abs4.increaseAbs();
        }
        YearGroup BUT1 = new YearGroup(new StudentAbs[]{abs1, abs2, abs3, abs4});
        BUT1.validation(5, 14);
    }
}