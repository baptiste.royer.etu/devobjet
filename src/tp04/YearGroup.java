package tp04;

public class YearGroup {
    private StudentAbs[] listeEtu;

    public YearGroup(StudentAbs[] liste) {this.listeEtu = liste;}
    public YearGroup() {this.listeEtu = new StudentAbs[0];}

    public StudentAbs[] getListeEtu() {
        return listeEtu;
    }

    public String getStringListeEtu() {
        String result = "[";
        for (int i = 0; i<this.listeEtu.length; i++) {
            result+=listeEtu[i].toString() + "\n";
        }
        return result + "]";
    }

    public void setListeEtu(StudentAbs[] liste) {
        this.listeEtu = liste;
    }

    public void addGrade(double[] grades) {
        for (int i = 0; i < this.listeEtu.length; i++) {
            this.listeEtu[i].getStud().addGrade(grades[i]);
        }
    }

    public void validation(int thresholdAbs, int thresholdAvg) {
        String lesGensQuiPassent = "";
        for (int i = 0; i < this.listeEtu.length; i++) {
            if (this.listeEtu[i].validation(thresholdAbs, thresholdAvg)) {
                lesGensQuiPassent+=listeEtu[i].toString() + "\n";
            }
        }
        System.out.println(lesGensQuiPassent.substring(0, lesGensQuiPassent.length() - 1));
    }
}