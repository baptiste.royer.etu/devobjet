package tp04;

public class NumeroAutomatique {
    private static int numero = 0;

    public static int getNumber() {
        return ++numero;
    }
}