package tp04;

public class StudentAbs {
    private Student stud;
    private int nbAbsence = 0;

    public StudentAbs(Student stud) {this.stud = stud;}
    public StudentAbs(String forename, String name, double[] grades) {this.stud = new Student(forename, name, grades);}
    public StudentAbs(String forename, String name) {this.stud = new Student(forename, name);}

    public Student getStud() {
        return this.stud;
    }

    public int getNbAbsences() {
        return this.nbAbsence;
    }

    public void increaseAbs() {
        this.nbAbsence++;
    }

    public String toString() {
        return this.stud.toString() + ", nbAbs = " + nbAbsence;
    }

    public boolean equals(StudentAbs other) {
        return (this.stud.equals(other.stud) && this.nbAbsence == other.nbAbsence);
    }

    public boolean warning(int thresholdAbs, double thresholdAvg) {
        return (this.nbAbsence >= thresholdAbs || this.stud.getAverage() <= thresholdAvg);
    }

    public boolean validation(int thresholdAbs, double thresholdAvg) {
        return (this.nbAbsence <= thresholdAbs && this.stud.getAverage() >= thresholdAvg);
    }
}