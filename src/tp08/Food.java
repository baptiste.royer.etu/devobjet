package tp08;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class Food implements IProduct, Comparable<Food> {
    private String label;
    private double price;
    private LocalDate bestBeforDate;
    private static int refIconnue = 0;

    public Food(String label, double price, LocalDate bestBeforDate) {
        if(label == null) {
            this.label = "refUnknown" + refIconnue;
            refIconnue++;
        } else {
            this.label = label;
        }
        this.price = price;
        this.bestBeforDate = bestBeforDate;
    }

    public Food(String label, double price) {
        if(label == null) {
            this.label = "refUnknown" + refIconnue;
            refIconnue++;
        } else {
            this.label = label;
        }
        this.price = price;
        this.bestBeforDate = LocalDate.now().plus(10, ChronoUnit.DAYS);
    }

    public String getLabel() {
        return this.label;
    }

    public double getPrice() {
        return this.price;
    }

    public LocalDate getBestBeforDate() {
        return this.bestBeforDate;
    }

    public boolean isPerishable() {
        return true;
    }

    public boolean isBestBefore(LocalDate date) {
        return this.bestBeforDate.isBefore(date);
    }

    @Override
    public int compareTo(Food arg0) {
        if(this.isBestBefore(arg0.getBestBeforDate())) {
            return 1;
        } else {
            return -1;
        }
    }

    public String toString() {
        return "[" + this.label + "=" + this.price + " -> before " + this.bestBeforDate + "]";
    }
}