package tp08;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class Shop {
    private ArrayList<Shelf> shelves;

    public Shop(){
        this.shelves = new ArrayList<Shelf>();
    }

    public Shop(ArrayList<Shelf> shelves){
        this.shelves = shelves;
    }

    public ArrayList<Shelf> getShelving(){
        return this.shelves;
    }

    public String toString() {
        String retour = "";
        for(Shelf shelf : this.shelves){
            retour += shelf.toString() + "\n";
        }
        return retour;
    }

    public void tidy(ArrayList<IProduct> products){
        for(int i = 0; i<products.size(); i++) {
            if(products.get(i).isPerishable()){
                for(Shelf shelf : this.shelves){
                    if(shelf.isRefregirated() && shelf.add(products.get(i))){
                        products.remove(i);
                        i--;
                        break;
                    }
                }
            } else {
                for(Shelf shelf : this.shelves){
                    if(!shelf.isRefregirated() && shelf.add(products.get(i))){
                        products.remove(i);
                        i--;
                        break;
                    }
                }
            }
        }
    }

    public ArrayList<IProduct> closeBestBeforeDate(int nbDays) {
        ArrayList<IProduct> retour = new ArrayList<IProduct>();
        for (Shelf shelf : this.shelves) {
            if(shelf.isRefregirated()) {
                for (IProduct product : shelf.getShelves()) {
                    if (((Food)product).isBestBefore(LocalDate.now().plus(nbDays, ChronoUnit.DAYS))) {
                        retour.add(product);
                    }
                }
            }
        }
        return retour;
    }
}