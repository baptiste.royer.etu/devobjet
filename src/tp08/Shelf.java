package tp08;
import java.util.ArrayList;

public class Shelf{
    private boolean refregirated;
    private int capacityMax;
    private ArrayList<IProduct> products;

    public Shelf(boolean refregirated, int capacityMax){
        this.refregirated = refregirated;
        this.capacityMax = capacityMax;
        this.products = new ArrayList<IProduct>();
    }

    public ArrayList<IProduct> getShelves(){
        return this.products;
    }

    public int getCapacityMax(){
        return this.capacityMax;
    }
    
    public boolean isFull(){
        return this.products.size() == this.capacityMax;
    }

    public boolean isEmpty(){
        return this.products.size() == 0;
    }

    public boolean isRefregirated(){
        return this.refregirated;
    }

    public String toString() {
        return "[" + isRefregirated() + ": " + getCapacityMax() + " -> " + getShelves().toString() + "]";
    }

    public boolean add(IProduct product){
        if(isFull()){
            return false;
        }
        this.products.add(product);
        return true;
    }

}