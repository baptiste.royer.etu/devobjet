package tp08;
public interface IProduct {

    public double getPrice();
    public boolean isPerishable();
}