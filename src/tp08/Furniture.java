package tp08;
public class Furniture implements IProduct {
    private String label;
    private double price;
    private static int refInconnue = 0;

    public Furniture(String label, double price) {
        if(label == null) {
            this.label = "refUnknown" + refInconnue;
            refInconnue++;
        } else {
            this.label = label;
        }
        this.price = price;
    }

    public String getLabel() {
        return this.label;
    }

    public double getPrice() {
        return this.price;
    }

    public boolean isPerishable() {
        return false;
    }

    public String toString() {
        return "[" + this.label + "=" + this.price + "]";
    }
}