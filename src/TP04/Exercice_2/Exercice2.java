package TP04.Exercice_2;

import java.util.Scanner;

public class Exercice2 {

    class LocalKeyboard {
        public static int readInt(String message) {
            Scanner scanner = new Scanner(System.in);
            System.out.println(message);
            int r = scanner.nextInt();
            scanner.close();
            return r;            
        }
    }

    public static int[] tab = {17, 12, 15, 38, 29, 157, 89, -22, 0, 5};

    public static int division(int index, int divisor) {
        return tab[index]/divisor;
    }

    public static void statement() {
        int x, y;
        x = LocalKeyboard.readInt("Write the index of the integer to divide: ");
        y = LocalKeyboard.readInt("Write the divisor: ");
        System.out.println("The result is: " + division(x,y));
    }
}