package TP04.Exercice_4;


public class Competitor {
    private int ID;
    private final String NAME;
    private Country nationality;
    private Team team;

    public Competitor(String name, Country nationality, Team team) {
        NumAuto temp = new NumAuto();
        this.ID = temp.getID();
        this.NAME = name;
        this.nationality = nationality;
        this.team = team;
    }

    public int getID() {
        return this.ID;
    }

    public String getName() {
        return this.NAME;
    }

    public Country getCountry() {
        return this.nationality;
    }

    public Team getTeam() {
        return this.team;
    }

    public String toString() {
        return this.ID + " - " + this.NAME + " (" + this.nationality.getCode() + ") -> " + this.team.toString();
    }

    public boolean isFrom(Team team) {
        return this.team == team;
    }

    public boolean isFrom(Country country) {
        return this.nationality == country;
    }

    public boolean equals(int ID) {
        return this.ID == ID;
    }
}