package TP04.Exercice_4;

public enum Country {
    FRANCE("FR"),
    GERMANY("GE"),
    RUSSIA("RU"),
    SWEDEN("SW"),
    AUSTRIA("AU"),
    ITALY("IT");

    private String code;

    Country(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }
}