package TP04.Exercice_3;

public class Thing {
    private String name;
    private int quantity;
    private Container container;

    public Thing(String name, int quantity, Container container) {
        this.name = name;
        this.quantity = quantity;
        this.container = container;
    }

    public String getName() {
        return name;
    }

    public int getQuantity() {
        return quantity;
    }

    public Container getContainer() {
        return container;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setContainer(Container container) {
        this.container = container;
    }

    @Override
    public String toString() {
        return "Thing{" +
                "name='" + name + '\'' +
                ", quantity=" + quantity +
                ", container=" + container +
                '}';
    }

    public void setQuantity(int value) throws ExtendedException {
        try {
            if(value < this.container.getCapacity()) {
                this.quantity = value;
            } else {
                throw new ExtendedException("Quantity too high to fit in the box.");
            }
        } catch(ExtendedException e) {
            System.out.println("Addition Impossible !");
        }
    }

    public boolean transferToLargerContainer() {
        if (this.container.ordinal() == Container.values().length - 1) {
            return false;
        }
        this.container = Container.values()[this.container.ordinal() + 1];
        return true;
    }

    public void add(int value) throws ExtendedException {
        try {
            setQuantity(value + this.getQuantity());
        } catch(ExtendedException e) {
            transferToLargerContainer();
            setQuantity(value + this.getQuantity());
        }
    }
}