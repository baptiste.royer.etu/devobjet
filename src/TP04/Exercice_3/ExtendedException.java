package TP04.Exercice_3;

public class ExtendedException extends Exception {
    public ExtendedException(String message) {
        super(message);
    }
}