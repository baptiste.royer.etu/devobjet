package TP04.Exercice_3;

public class UseThing {
    public static void main(String[] args) throws ExtendedException {
        Thing ch1 = new Thing("uneThing", 3, Container.BOX);
        System.out.println(ch1);
        ch1.add(20);
        System.out.println(ch1 + "\n");
        Thing ch2 = new Thing("maThing", 12, Container.BOX);
        System.out.println(ch2);
        ch2.add(60);
        System.out.println(ch2 + "\n");
    }
}