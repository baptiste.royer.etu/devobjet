package TP04.Exercice_3;

public enum Container {
    BLISTER(1),
    BOX(10),
    CRATE(50);

    private final int capacity;

    Container(int capacity) {
        this.capacity = capacity;
    }

    public int getCapacity() {
        return this.capacity;
    }
}