package TP04.Exercice_1.Exemple3;

import TP04.Exercice_1.Exemple2.MyException;

public class Exemple3 {
    public void doItAgain() throws MyException {
        throw new MyException();
    }

    public void example3() {
        try {
           doItAgain();
        } catch (Exception e) {
           //processing(); //N'existe pas ta grand mère la cheffe cuistot.
        }
    }
}