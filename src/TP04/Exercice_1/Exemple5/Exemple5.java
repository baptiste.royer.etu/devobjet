package TP04.Exercice_1.Exemple5;

public class Exemple5 {
    public void example5() {
        doItFinally();
    }
    
    public int doItFinally() {
        throw new RuntimeException();
    }
}

/*
 * Aucune erreur ici va savoir pourquoi...
 */