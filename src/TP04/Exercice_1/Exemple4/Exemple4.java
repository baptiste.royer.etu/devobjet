package TP04.Exercice_1.Exemple4;    
    
public class Exemple4 {    
    public int example4() {
        int data = 0; // Initialize the variable with a default value
        try {
            data = getData();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return data;
    }

    public int getData() throws NullPointerException {
            throw new NullPointerException();
    }
}
